apiVersion: {{ include "common.capabilities.deployment.apiVersion" . }}
kind: Deployment
metadata:
  name: {{ include "dependabot-gitlab.fullname" . }}-web
  labels:
    {{- include "dependabot-gitlab.labels" . | nindent 4 }}
  {{- with .Values.web.deploymentAnnotations }}
  annotations:
    {{- toYaml . | nindent 4 }}
  {{- end }}
spec:
  strategy: {{ toYaml .Values.web.updateStrategy | nindent 4 }}
  replicas: {{ .Values.web.replicaCount }}
  selector:
    matchLabels:
      {{- include "dependabot-gitlab.selectorLabels" . | nindent 6 }}
      app.kubernetes.io/component: web
  template:
    metadata:
      labels:
        {{- include "dependabot-gitlab.selectorLabels" . | nindent 8 }}
        app.kubernetes.io/component: web
      annotations:
        {{- include "dependabot-gitlab.podAnnotations" . | nindent 8 }}
        {{- with .Values.web.podAnnotations }}
        {{ toYaml . | nindent 8 }}
        {{- end }}
    spec:
      serviceAccountName: {{ include "dependabot-gitlab.serviceAccountName" . }}
      {{- with .Values.podSecurityContext }}
      securityContext:
        {{- toYaml . | nindent 8 }}
      {{- end }}
      initContainers:
        {{- include "dependabot-gitlab.migrationsWaitContainer" . | nindent 8 }}
        {{- include "dependabot-gitlab.redisWaitContainer" . | nindent 8 }}
      containers:
        - name: {{ .Chart.Name }}-web
          {{- include "dependabot-gitlab.image" . | nindent 10 }}
          args:
            - "rails"
            - "server"
          env:
          {{- with (include "dependabot-gitlab.database-credentials" .) }}
            {{- . | nindent 12 }}
          {{- end }}
          {{- with .Values.web.extraEnvVars }}
            {{- toYaml . | nindent 12 }}
          {{- end}}
          envFrom:
            - configMapRef:
                name: {{ include "dependabot-gitlab.fullname" . }}
            - secretRef:
                {{- if .Values.credentials.existingSecret }}
                name: {{ .Values.credentials.existingSecret }}
                {{- else }}
                name: {{ include "dependabot-gitlab.fullname" . }}
                {{- end }}
            {{- if (include "dependabot-gitlab.registries-credentials" .) }}
            - secretRef:
                {{- if .Values.registriesCredentials.existingSecret }}
                name: {{ .Values.registriesCredentials.existingSecret }}
                {{- else }}
                name: {{ include "dependabot-gitlab.fullname" . }}-registries
                {{- end }}
            {{- end }}
          ports:
            - name: http
              containerPort: {{ .Values.service.port }}
              protocol: TCP
          {{- if .Values.web.livenessProbe.enabled }}
          livenessProbe:
            httpGet:
              path: /healthcheck
              port: http
            failureThreshold: {{ .Values.web.livenessProbe.failureThreshold }}
            periodSeconds: {{ .Values.web.livenessProbe.periodSeconds }}
            timeoutSeconds: {{ .Values.web.livenessProbe.timeoutSeconds }}
          {{- end }}
          {{- if .Values.web.livenessProbe.enabled }}
          startupProbe:
            httpGet:
              path: /healthcheck
              port: http
            failureThreshold: {{ .Values.web.startupProbe.failureThreshold }}
            periodSeconds: {{ .Values.web.startupProbe.periodSeconds }}
            timeoutSeconds: {{ .Values.web.startupProbe.timeoutSeconds }}
            initialDelaySeconds: {{ .Values.web.startupProbe.initialDelaySeconds }}
          {{- end }}
          {{- with .Values.web.resources }}
          resources:
            {{- toYaml . | nindent 12 }}
          {{- end }}
          {{- with .Values.web.extraVolumeMounts }}
          volumeMounts:
            {{- toYaml . | nindent 10 }}
          {{- end }}
      {{- with .Values.nodeSelector }}
      nodeSelector:
        {{- toYaml . | nindent 8 }}
      {{- end }}
      {{- with .Values.web.affinity }}
      affinity:
        {{- toYaml . | nindent 8 }}
      {{- end }}
      {{- with .Values.web.tolerations }}
      tolerations:
        {{- toYaml . | nindent 8 }}
      {{- end }}
      {{- with .Values.image.imagePullSecrets }}
      imagePullSecrets:
      {{- toYaml . | nindent 8}}
      {{- end }}
      {{- with .Values.web.extraVolumes }}
      volumes:
        {{- toYaml . | nindent 8 }}
      {{- end }}
